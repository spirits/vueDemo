import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index/Index'
import View from '@/components/Index/View'
Vue.use(Router)

export default new Router({
  mode:"history",
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta:{
        requiresAuth:true
      }
    },
    {
      path:'/zixun/:type/:id',
      name:'zixun',
      component: View,
      meta:{
        requiresAuth:true
      }
    }
  ]
})
