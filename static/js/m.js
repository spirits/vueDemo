var show1=0;
var pai=0;
var comment=0;

$(function(){
	var bodyH=parseInt($(document).height());
//搜索
	var search=0;
	$(".m_search_box").height($(document).height()-$(".m_top").height());
	$(".m_top .m_search").click(function(){
		if(search==0){
			$(".m_search_box").show();
			$(".m_top .m_search").addClass("current");
			$(".m_top .m_search").html("取消");
			search=1;
		}else{
			$(".m_search_box").hide();
			$(".m_top .m_search").removeClass("current");
			$(".m_top .m_search").html("");
			search=0;
		}
	})
//返回顶部
	$(window).scroll(function(){
		if($(window).scrollTop()>300){
			$(".m_goTop").show();
		}else{
			$(".m_goTop").hide();
		}
	})
	$(".m_goTop").click(function(){
		$("html,body").animate({scrollTop:0},500);
	})
//分类
	if($(".m_classify").length>0){
		var topH=parseInt($(".m_classify dl").offset().top+$(".m_classify dl").height()+2);
		$(".m_classify div").height(bodyH-topH);	
		$(".m_classify dt").click(function(){
			if(show1==0){
				$(this).addClass("up");
				$(".m_classify div").show();
				show1=1;
			}else{
				$(this).removeClass("up");
				$(".m_classify div").hide();
				show1=0;
			}
		})
	}
//介绍
	$(".show_btn").attr("con","0");
	$(".show_btn").click(function(){
		var con = $(this).attr('con');
		if(con=="0"){
			$(this).find("img").attr("src",$(this).find("img").attr("hideT"));
			$(this).prev().addClass("con_show");
			$(this).attr('con',"1");
		}else if(con=="1"){
			$(this).find("img").attr("src",$(this).find("img").attr("showT"));
			$(this).prev().removeClass("con_show");
			$("html,body").animate({scrollTop:$(this).prev().offset().top-100},500);
			$(this).attr('con',"0");
		}
	})
//注册验证码
	var countdown=60;
	var down_time=null;
	$(".yanzhengma").click(function(){
		var This=this;
		if($(This).hasClass("forbidden")){
			return;
		}
		$(This).addClass("forbidden");
		down_time=setInterval(function(){
			if(countdown>1){
				countdown--;
				$(This).text(countdown+"s后重发");
			}else{
				$(This).removeClass("forbidden");
				$(This).text("获取验证码");
				clearInterval(down_time);
				countdown=60;
				down_time=null;
			}			
		}, 1000);
	})
//排序方式
	$(".huida_title dd .pai").click(function(){
		if(pai==0){
			$(this).next().fadeIn();
			pai=1;
		}else{
			$(this).next().fadeOut();
			pai=0;
		}
	})
//内容伸缩
	$(".huati_huida_list .con .show").click(function(){
		$(this).parent().fadeOut();
		$(this).parent().next().fadeIn();
	})
	$(".huati_huida_list .con_all .hide").click(function(){
		$("html,body").animate({scrollTop:$(this).parent().parent().offset().top},200);
		$(this).parent().fadeOut();
		$(this).parent().prev().fadeIn();
	})
//评论
	var this_html;
	$(".huati_huida_list .comment p").click(function(){
		if(comment==0){
			this_html=$(this).html();
			$(this).html("收起评论<i></i>");
			$(this).find("i").show();
			$(this).parent().next(".comment_hide_box").fadeIn();
			comment=1;
		}else{
			$(this).html(this_html);
			$(this).find("i").hide();
			$(this).parent().next(".comment_hide_box").fadeOut();
			comment=0;
		}
	})
//编辑框
	$(".edit_form textarea").focus(function(){
		$(".edit_bottom dt").show();
	}).blur(function(){
		$(".edit_bottom dt").hide();
	})
	$(".edit_bottom dt").click(function(){
		$(this).hide();
	})
	$(".edit_bottom dd span").click(function(){
		$(this).toggleClass("current");
	})
//选择分类
	$(".huati_all>dd>span").click(function(){
		$(this).toggleClass("current");
		if($(this).hasClass("current")){
			$(".huati_choose dd label").hide();
			$(".huati_choose dd").append("<span rel='"+$(this).text()+"'>"+$(this).text()+"</span>");
		}else{
			$(".huati_choose dd span[rel="+$(this).text()+"]").remove();
		}
		if($(".huati_choose dd span").length>0){
			$(".huati_choose dt").show();
		}else{
			$(".huati_choose dt").hide();
			$(".huati_choose dd label").show();
		}
	})
	$(".huati_choose dt").click(function(){
		$(this).hide();
		$(".huati_choose dd span").remove();
		$(".huati_choose dd label").show();
		$(".huati_all>dd>span").removeClass("current");
	})
	$(".huati_choose dd").on('click','span',function(){
		$(this).remove();
		$(".huati_all dd span[rel="+$(this).text()+"]").removeClass("current");
		if($(".huati_choose dd span").length>0){
			$(".huati_choose dt").show();
		}else{
			$(".huati_choose dt").hide();
			$(".huati_choose dd label").show();
		}
	})
//错误提示
	setTimeout(function(){
		$(".error_alert").hide();
	},5000);
//懒加载
	if($(".lazy").length > 0){
		$(".lazy").delayLoading({
			defaultImg: "../new_img/load.gif",           // 预加载前显示的图片
			errorImg: "../new_img/defaultpic.gif",                        // 读取图片错误时替换图片(默认：与defaultImg一样)
			imgSrcAttr: "originalSrc",           // 记录图片路径的属性(默认：originalSrc，页面img的src属性也要替换为originalSrc)
			beforehand: 0,                       // 预先提前多少像素加载图片(默认：0)
			event: "scroll",                     // 触发加载图片事件(默认：scroll)
			duration: "normal",                  // 三种预定淡出(入)速度之一的字符串("slow", "normal", or "fast")或表示动画时长的毫秒数值(如：1000),默认:"normal"
			container: window,                   // 对象加载的位置容器(默认：window)
			success: function (imgObj) { },      // 加载图片成功后的回调函数(默认：不执行任何操作)
			error: function (imgObj) { }       // 加载图片失败后的回调函数(默认：不执行任何操作)
		});
	}
})

$(document).bind("touchend",function(e){
	var target = $(e.target);
	if(target.closest(".m_classify div ul,.m_classify dt").length == 0){
		$(".m_classify div").fadeOut();
		show1=0;
	};
	if(target.closest(".huida_title dd .pai").length == 0){
		$(".huida_title dd div").fadeOut();
		pai=0;
	};
})

