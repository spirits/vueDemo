$(function(){
    var upglide_time=3000;
    var count=0;
    var count2=0;
    var timer =null;
    var btn_boxName="upglide_list";
    var upglide=$(".upglide");
    var upglide_ul=upglide.find("ul");
    var upglide_li=upglide.find("li");
    var upglide_length=upglide_li.length;
    var upglide_height=upglide.height();
    var upglide_btn='<div class="'+btn_boxName+'"><div>';
    upglide_li.each(function(){
        upglide_btn+='<p><a>'+$(this).find("img").attr("alt")+'</a><span></span></p>';
    })
    upglide_btn+="</div></div>";
    upglide.append(upglide_btn);

    var btn_box=$("."+btn_boxName);
    btn_box.find("p:eq(0)").addClass("current");

    $(btn_box).find("div > p").hover(function(){
      $(this).find("span").stop().animate({"width":"100%"},300);
    },function(){
        $(this).find("span").stop().animate({"width":"0"},300);
    })

    btn_box.find("div>p").click(function(){
        clearInterval(timer);
        count=count2=$(this).index();
        moveUP(count,count2);
        setTimeout(run,0);
    })

    function run(){
        timer=setInterval(function(){
            count++;
            count2++;        
            if(count2==upglide_length){
                count2=0;
            }
            moveUP(count,count2);    
        },upglide_time);
    }
  
    run();
    function moveUP(num,num2){
        btn_box.find("div>p").removeClass("current");
        btn_box.find("div>p").eq(num2).addClass("current");
        var top=num*upglide_height;
        upglide_ul.animate({"top":"-"+top},300,function(){
            if(num==upglide_length-1){
                upglide_ul.append(upglide_ul.find("li:eq(0)").clone());
            }
            if(num==upglide_length){
                count=0;
                upglide_ul.css("top","0");
                upglide_ul.find("li:last").remove();
            } 
        });
    }
})
