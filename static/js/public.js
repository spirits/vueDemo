
	$(".kefu_btn").attr({"href":"###","target":"_blank"});
	$(".goTop").click(function(){
		$("html,body").animate({scrollTop:0},500);
	})
	$(window).scroll(function(){
		if($(window).scrollTop()>300){
			$(".goTop").show();
		}else{
			$(".goTop").hide();
		}
	})
//播放记录
    $(".play_record").hover(function(){
        $(this).find(".box").show();
    },function(){
        $(this).find(".box").hide();
    })
//导航
	$(".nav_list li").hover(function(){
		$(this).find(".hide_nav").stop(true,true).slideDown();
		$(this).find(".icon").show();
	},function(){
 		$(this).find(".hide_nav").slideUp();
 		$(this).find(".icon").hide();
 	})

//右侧浮动按钮
 	$(".right_btn li").hover(function(){
 		$(this).find("p").stop(true,true).slideDown();
 	},function(){
 		$(this).find("p").slideUp();
 	})
 	$(".right_btn li p").hover(function(){
 		$(this).parent().find("a").addClass("hover");
 	},function(){
 		$(this).parent().find("a").removeClass("hover");
 	})

//登陆注册
 	$(".user_entrance .denglu").click(function(){
 		$(".entrance_box .denglu").addClass("current");
 		$(".shadow").show();
 		$(".entrance").show();
 		$(".entrance_box").fadeIn();
 	})

 	$(".user_entrance .zhuce").click(function(){
 		$(".entrance_box .zhuce").addClass("current");
 		$(".shadow").show();
 		$(".register").show();
 		$(".entrance_box").fadeIn();
 	})

 	$(".entrance_box>dt>p>span").click(function(){
 		$(this).siblings("span").removeClass("current");
 		$(this).addClass("current");
 	})

 	$(".entrance_box .denglu").click(function(){
 		$(".entrance_box dd").hide();
 		$(".entrance").show();
 	})

 	$(".entrance_box .zhuce").click(function(){
 		$(".entrance_box dd").hide();
 		$(".register").show();
 	})

 	$(".entrance_box .close").click(function(){
 		$(".shadow").hide();
 		$(".entrance_box").fadeOut();
 		$(".entrance_box>dt>p>span").removeClass("current");
 		$(".entrance_box dd").hide(500);
 	})
//注册验证码
	var countdown=60;
	var down_time=null;
	$(".yanzhengma").click(function(){
		var This=this;
		if($(This).hasClass("forbidden")){
			return;
		}
		$(This).addClass("forbidden");
		down_time=setInterval(function(){
			if(countdown>1){
				countdown--;
				$(This).text(countdown+"s后重发");
			}else{
				$(This).removeClass("forbidden");
				$(This).text("获取验证码");
				clearInterval(down_time);
				countdown=60;
				down_time=null;
			}			
		}, 1000);
	})
//支付方式
	$(".pay_box>.btn>p>a").click(function(){
		$(this).addClass("current").siblings("a").removeClass("current");
	})
//修改昵称
	$(".user_head .edit").click(function(){
		$(this).parent().hide();
		$(this).parent().siblings("form").css("display","table-cell");
	})
	$(".user_head .cancel").click(function(){
		$(this).parent().hide();
		$(this).parent().siblings("p").show();
		$(this).siblings("input").val($(this).siblings("input").attr("rel"));
	})
//修改号码
	$(".user_number .edit").click(function(){
		$(this).siblings("input").removeAttr("disabled"); 
		$(this).hide().siblings().show();
	})
	$(".user_number .cancel").click(function(){
		$(this).hide();
		$(this).siblings("button").hide();
		$(this).siblings(".edit").show();
		$(this).siblings("input").attr("disabled","disabled"); 
		$(this).siblings("input").val($(this).siblings("input").attr("rel"));
	})
//错误提示
	setTimeout(function(){
		$(".error_alert").hide();
	},5000);
//懒加载
	if($(".lazy").length > 0){
		$(".lazy").delayLoading({
			defaultImg: "../new_img/load.gif",           // 预加载前显示的图片
			errorImg: "../new_img/defaultpic.gif",                        // 读取图片错误时替换图片(默认：与defaultImg一样)
			imgSrcAttr: "originalSrc",           // 记录图片路径的属性(默认：originalSrc，页面img的src属性也要替换为originalSrc)
			beforehand: 0,                       // 预先提前多少像素加载图片(默认：0)
			event: "scroll",                     // 触发加载图片事件(默认：scroll)
			duration: "normal",                  // 三种预定淡出(入)速度之一的字符串("slow", "normal", or "fast")或表示动画时长的毫秒数值(如：1000),默认:"normal"
			container: window,                   // 对象加载的位置容器(默认：window)
			success: function (imgObj) { },      // 加载图片成功后的回调函数(默认：不执行任何操作)
			error: function (imgObj) { }       // 加载图片失败后的回调函数(默认：不执行任何操作)
		});
	}
})
