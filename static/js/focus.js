$(document).ready(function(){
	var btn = "<div class='flicking_con'><p>";
	$(".main_image li").each(function(){
		btn += "<span></span>";
	})
	btn +="</p></div>";
	$(".m_focus").append(btn);
	$(".m_focus").append('<a id="btn_prev"></a><a id="btn_next"></a>');
	
	$(".main_image").touchSlider({
		flexible : true,
		speed : 200,
		btn_prev : $("#btn_prev"),
		btn_next : $("#btn_next"),
		paging : $(".flicking_con span"),
		counter : function (e){
			$(".flicking_con span").removeClass("on").eq(e.current-1).addClass("on");
		}
	});	

	setInterval(function(){
		$("#btn_next").click();
	}, 5000);
});